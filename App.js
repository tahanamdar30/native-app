import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

//Compo
import About from './Compo/About';
import Profile from './Compo/Profile';
import Us from './Compo/TodoList/Us';
import One from './Compo/One';
import Two from './Compo/Two';
//TabNavigation
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const TabNav = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        labelStyle: {fontSize: 14},
      }}>
      <Tab.Screen
        name="Home"
        component={One}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <Icon name="fort-awesome" size={30} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Two}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => <Icon name="user" size={30} color={color} />,
        }}
      />
    </Tab.Navigator>
  );
};

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {backgroundColor: 'gray'},
          headerTitleAlign: 'center',
        }}>
        <Stack.Screen
          name="MyApp"
          component={TabNav}
          options={{headerTitle: 'خانه'}}
        />
        <Stack.Screen
          name="About"
          component={About}
          options={{headerTitle: 'درباره من '}}
        />

        <Stack.Screen
          name="TodoList"
          component={Us}
          options={{headerTitle: 'لیست'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

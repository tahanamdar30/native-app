import React from 'react';
import {Image, View, StyleSheet, Text} from 'react-native';
const Two = () => {
  return (
    <View style={styles.continer}>
      <Image
        source={{
          uri: 'https://reactjs.org/logo-og.png',
          cache: 'only-if-cached',
        }}
        style={{width: 100, height: 100, borderRadius: 100 / 2}}
      />
      <Text style={styles.txt}>Taha Namdar</Text>
      <Text style={styles.paragraph}>
        It is a long established fact that a reader will be distracted by the
        readable content of a page when looking at its layout. The point of
        using Lorem Ipsum is that it has a more-or-less normal distribution of
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    paddingTop: 40,
    alignItems: 'center',
    backgroundColor: '#FFC2B4',
    height: 1000,
  },
  txt: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingTop: 10,
  },
  paragraph: {
    paddingTop: 20,
    justifyContent: 'center',
    margin: 10,
    textAlign: 'justify',
  },
});

export default Two;

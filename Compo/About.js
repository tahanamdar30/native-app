import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
const About = () => {
  return (
    <View style={styles.container}>
      <Text>
        Curabitur sed ipsum nisl. Pellentesque at risus auctor, scelerisque nisi
        at, laoreet lorem. Maecenas faucibus, felis sed sollicitudin laoreet,
        leo tellus consectetur quam, et dignissim tellus sem at augue. Nulla est
        augue, sodales id molestie quis, luctus vel lorem. Donec magna augue,
        pharetra vitae ex vitae, vulputate suscipit lacus. Maecenas semper vel
        nunc at dapibus. Nulla nec placerat purus. Nulla vulputate nulla in
        metus ornare ullamcorper. Proin nec tortor sit amet est vehicula ornare
        ultrices sed ex. Aenean fermentum felis et mauris auctor, id fermentum
        turpis vestibulum. Vestibulum rhoncus, dui vitae mollis scelerisque,
        lorem lorem viverra lacus, quis facilisis ipsum magna sit amet mauris.
        Vestibulum tellus lectus, placerat non tellus nec, elementum condimentum
        sem. Pellentesque fringilla nec justo at elementum. Fusce sit amet nisi
        ut felis ultricies rutrum at eu diam. Nulla dolor leo, ornare id mattis
        quis, interdum sed quam.
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#FFC2B4',
    height: 1000,
  },
});

export default About;

import {firebrick} from 'color-name';
import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
const ListItem = props => {
  return (
    <TouchableOpacity style={styles.ListItem}>
      <View style={styles.ListItemChild}>
        <Text style={styles.font}>{props.title}</Text>
        <Icon
          name="close"
          size={20}
          color={firebrick}
          onPress={() => props.deleteItem(props.id)}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  ListItem: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: '#F9ECEC',
    backgroundColor: '#FFC2B4',
  },

  ListItemChild: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    textAlign: 'center',
  },
  font: {
    fontSize: 20,
  },
});

export default ListItem;

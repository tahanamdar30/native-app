import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const AddItems = props => {
  const [text, setText] = useState('');

  const onChange = textValue => setText(textValue);

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Add Item..."
        style={styles.input}
        onChangeText={onChange}
      />
      <TouchableOpacity style={styles.btn} onPress={() => props.addItem(text)}>
        <Text style={styles.text}>
          <Icon name="plus" size={20} />
          Add Item
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },

  input: {
    backgroundColor: '#f8f8f8',
    padding: 10,
  },

  btn: {
    backgroundColor: '#FB3640',
    justifyContent: 'center',
    textAlign: 'center',
    flexDirection: 'row',
    padding: 10,
    margin: 8,
  },

  text: {color: 'white', fontSize: 22},
});

export default AddItems;

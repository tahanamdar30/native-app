import * as React from 'react';
import {
  Button,
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const One = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <Button
          onPress={() => navigation.navigate('About')}
          title="About Me"
          color="#000000"
          style={styles.btn}
        />
      </View>

      <View style={styles.buttonContainer}>
        <Button
          title="TodoList"
          color="#000000"
          onPress={() => navigation.navigate('TodoList')}
          style={styles.btn}
        />
      </View>
      <View style={styles.image}>
        <Image
          source={{
            uri: 'https://icon-library.com/images/hi-icon/hi-icon-0.jpg',
            cache: 'only-if-cached',
          }}
          style={{width: 255, height: 244}}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Dimensions.get('window').paddingTop < 350 ? 20 : 100,
    flexDirection: 'row',
    backgroundColor: '#DB2D43',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  buttonContainer: {
    margin: 4,
    width: 130,
  },
  image: {
    paddingTop: Dimensions.get('window').paddingTop < 350 ? 10 : 80,
  },
});

export default One;

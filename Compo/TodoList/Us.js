import React, {useState} from 'react';
import ListItem from './ListItem';
import {View, StyleSheet, FlatList, ScrollView} from 'react-native';
import AddItems from './AddItem';
import uuid from 'react-native-uuid';
const Us = () => {
  const [data, setData] = useState([]);

  const deleteItem = id => {
    setData(prevItems => {
      return prevItems.filter(item => item.id != id);
    });
  };
  const addItem = title => {
    setData(prevItems => {
      return [{title, id: uuid.v4()}, ...prevItems];
    });
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <AddItems addItem={addItem} />
        <FlatList
          data={data}
          renderItem={({item}) => (
            <ListItem title={item.title} deleteItem={deleteItem} id={item.id} />
          )}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});

export default Us;
